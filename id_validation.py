# 英文
local_table = {'A':10,'B':11,'C':12,'D':13,'E':14,'F':15,'G':16,'H':17,'I':34,
        'J':18,'K':19,'L':20,'M':21,'N':22,'O':35,'P':23,'Q':24,'R':25,
        'S':26,'T':27,'U':28,'V':29,'W':32,'X':30,'Y':31,'Z':33}
id_number = input('請輸入身份證字號: ')
check = False

while True:
  id_arr = list(id_number)
  if len(id_arr) != 10: break
  local = str(local_table[id_arr[0]])

  # 英文檢查碼
  check_arr = list(local)
  check_arr[0] = int(check_arr[0]) * 1
  check_arr[1] = int(check_arr[1]) * 9

  # 性別
  sex = id_arr[1]
  if sex != '1' and sex != '2': break
  check_arr.append(int(sex) * 8)

  # 第3~9流水碼
  for i in range(7):
    check_arr.append(int(id_arr[i+2]) * (7 - i))

  # 第十碼
  last_num = int(id_arr[9])
  check_num = 10 - sum(check_arr)%10

  if check_num == 10:
    check_num = 0
  if last_num != check_num: break
  check = True
  break

if check == False:
  print('身分證錯誤')
else: 
  print('身分證正確')