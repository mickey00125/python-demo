import random
from tabnanny import check

# 英文
local_table = {'A':10,'B':11,'C':12,'D':13,'E':14,'F':15,'G':16,'H':17,'I':34,
        'J':18,'K':19,'L':20,'M':21,'N':22,'O':35,'P':23,'Q':24,'R':25,
        'S':26,'T':27,'U':28,'V':29,'W':32,'X':30,'Y':31,'Z':33}
local = random.choice(list(local_table.keys()))

# 英文檢查碼
check_arr = list(str(local_table[local]))
check_arr[0] = int(check_arr[0]) * 1
check_arr[1] = int(check_arr[1]) * 9

# 性別
sex = random.randint(1, 2)
check_arr.append(sex * 8)

# 第3~9流水碼
nums_str = ''
for i in range(7):
  nums = random.randint(0, 9)
  nums_str = nums_str + str(nums)
  check_arr.append(nums * (7 - i))

# 第十碼
check_num = 10 - sum(check_arr)%10
if check_num == 10:
  check_num = 0

id = str(local) + str(sex) + nums_str + str(check_num)

print(id)