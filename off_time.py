import requests
import schedule
import time

def doJob():
  msg = '幾點下班: '

  # line 推播
  url = "https://notify-api.line.me/api/notify"
  payload={'message':{msg}}
  headers = {'Authorization': 'Bearer ' + 'sH6NflyaU32QWs2vx6L1s0HOlbHOAWL9WdDnJnjV1HI'}
  response = requests.request("POST", url, headers=headers, data=payload)
  print(response.text)

# schedule.every(5).seconds.do(doJob)
# 每天7.30執行
schedule.every().day.at("17:40").do(doJob)

while True:
  schedule.run_pending()
  time.sleep(1)