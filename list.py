# list1 = []
# list1.append('1')
# list1.append('two')

# list2 = ['2', '3', '4']

# # 加入多個元素
# list1.extend(list2)

# # 串接兩個清單
# print(list1)

# 輸入半徑，計算圓周長和面積
# r = int(input('請輸入半徑: '))
# pi = 3.14
# length = pi * r * 2
# area = pi * r * r
# print('圓周長: ', length)
# print('面積: ', area)

# 百分製成績轉換為等級製成績
# f = float(input('請輸入分數: '))
# if f >= 90:
#   print('A')
# elif f >= 80:
#   print('B')
# elif f >= 70:
#   print('C')
# elif f >= 60:
#   print('D')
# else:
#   print('E')

# 打印三角形陣列
# for i in range(1, 6, 1):
#     print('*' * i)
# for i in range(5, 0, -1):
#     print('*' * i)
# for i in range(1, 6, 1):
#     print(' ' * (5 - i), '*' * i)

# lt = [ i for i in range(1, 11) if i % 2 == 0]
# lt2 = lt
# lt3 = [(a for a in lt) * (b for b in lt2)]
# print(lt3)


# 判斷奇偶數
# n = int(input('請輸入數字: '))
# if n % 2 == 0:
#   print('偶數')
# else:
#   print('奇數')

# 計算字數
# text = str(input('請輸入文字: '))
# length = len(text)
# print(length)

# 攝氏華氏轉換
# c = int(input('1是攝氏，2是華氏，請選擇單位: '))
# n = int(input('請輸入溫度: '))

# if c == 1:
#   t = round(9/5 * n + 32)
#   print(f'攝氏 {n} 度 = 華氏 {t} 度')
# else: 
#   t = round(5/9 * (n - 32))
#   print(f'華氏 {n} 度 = 攝氏 {t} 度')

# 公分/英吋換算
# c = int(input('1是公分，2是英吋，請選擇單位: '))
# n = int(input('請輸入長度: '))

# if c == 1:
#   t = 0.394 * n
#   print(f'{n} 公分 = {t} 英吋')
# else: 
#   t = n / 0.394
#   print(f'{n} 英吋 = {t} 公分')

# 平年與閏年
# y = int(input('請輸入年分: '))
# if y % 4 == 0: 
#   if y % 100 == 0:
#     if y % 400 == 0: 
#       print(f'{y}為閏年')
#     else:
#       print(f'{y}為平年')
#   else:
#     print(f'{y}為閏年')    
# else:
#   print(f'{y}為平年')

# 找出中間的字元
# t = str(input('請輸入字串: '))
# l = len(t)
# lt = list(t)

# if l % 2 == 0:
#   i = int(l / 2)
# else:
#   i = int(l / 2)

# print(lt[i])

# 1~49大樂透選6組號碼
# import random
# a = random.sample(range(1, 50), 6)
# print(a)


# loading進度條
# import time
# n = 10

# for i in range(n + 1):
#   print(f'\r[{"██"*i}{"  "*(n-i)}] {i*100/n}%', end='')
#   time.sleep(1)


# 猜數字
# import random
# a = random.randint(1, 50)
# n = int(input('請輸入數字: '))

# try: 
#   if n <= 50 and n >=1:
#     while n != a:
#       if n < a:
#         n = int(input(f'再大一點: '))
#       elif n > a:
#         n = int(input(f'再小一點: '))
#     else:
#       print('恭喜猜對!')
#   else:
#     raise ValueError('請輸入1~50之間的數字')
# except ValueError as msg:
#   print(msg)


# 幾A幾B
# import random
# anwser = random.sample(range(1, 10), 4)
# a = b = n = 0

# while a != 4:
#   a = b = n = 0
#   user = list(input('請輸入四個數字:'))
#   print(user)
#   for i in user:
#     if int(user[n]) == anwser[n]:
#       a += 1
#     else: 
#         if int(i) in anwser:
#           b += 1
#     n += 1
#   print(f'{a}A{b}B')
# print(f'答對了! 答案: {anwser}')


# 時鐘
# import time
# import datetime

# while True:
#   t = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
#   print(f'\r現在時間: {t}', end='')
#   time.sleep(1)

# 計算BMI
# h = float(input('請輸入身高: ')) / 100
# w = float(input('請輸入體重: ')) 

# bmi = round((w / (h * h)), 2)

# if bmi > 25:
#   alert = 'BMI超標'
# elif bmi < 18.5:
#   alert = 'BMI過低'
# else:
#   alert = '正常'


# print(f'BMI為 {bmi}，{alert}')

# 排程
# import schedule
# import time

# def testJob():
#     print('test')
# schedule.every(5).seconds.do(testJob)
# while True:
#     schedule.run_pending()
#     time.sleep(1)

# 爬蟲發票兌獎
# import requests

# url = "https://invoice.etax.nat.gov.tw/index.html"
# web = requests.get(url)
# web.encoding = 'utf-8'

# from bs4 import BeautifulSoup
# soup = BeautifulSoup(web.text, "html.parser")
# td = soup.select('.container-fluid')[0].select('.etw-tbiggest')
# ns = td[0].getText()
# n1 = td[1].getText()
# n2 = [td[2].getText()[-8:], td[3].getText()[-8:], td[4].getText()[-8:]]

# while True:
#   num = input('請輸入末三碼: ')
  
#   if num == ns:
#     print('中1000萬!')
#   if num == n1:
#     print('中200萬!')
#   for i in n2:
#     if num == i[-7:]:
#       print('中40000!')
#     if num == i[-6:]:
#       print('中10000!')
#     if num == i[-5:]:
#       print('中4000!')
#     if num == i[-4:]:
#       print('中1000!')
#     if num == i[-3:]:
#       print('中200!')

