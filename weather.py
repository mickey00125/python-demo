import requests
import schedule
import time

def getTemp():
  api_key = 'e43a94a147c358325d377e7f2c8a6c47'
  url = f'https://api.openweathermap.org/data/2.5/weather?id=1665148&appid={api_key}&units=metric'
  data = requests.get(url)
  data_json =  data.json()['main']

  temp = round(data_json['temp'])
  feels_like = round(data_json['feels_like'])
  temp_max = round(data_json['temp_max'])
  temp_min = round(data_json['temp_min'])

  msg = f'當前溫度: {temp}\n體感溫度: {feels_like}\n最高溫度: {temp_max}\n最低溫度: {temp_min}'

  return msg

temp = getTemp()
  
def sendToLine(temp):
  #line推播
  url = "https://notify-api.line.me/api/notify"
  payload={'message':{temp}}
  headers = {'Authorization': 'Bearer ' + 'sH6NflyaU32QWs2vx6L1s0HOlbHOAWL9WdDnJnjV1HI'}
  response = requests.request("POST", url, headers=headers, data=payload)
  print(response.text)

# 每天7.30執行
schedule.every().day.at("07:30").do(sendToLine, temp)

while True:
    schedule.run_pending()
    time.sleep(1)